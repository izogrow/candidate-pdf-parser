from pdfminer.high_level import extract_text
import requests
import camelot

URL_TO_SAMSMU_PDF = "https://samsmu.ru/files/priem/2021/ks_sb_0208_18.pdf"
TARGET_ID = "169-886-415 49"


def download_pdf(url, title):
    r = requests.get(url, allow_redirects=True, verify=False)
    open(title, 'wb').write(r.content)


"""
accept_strategy - 
"""
def find_numbers_of_people_up_to_target(path_to_file):
    text = extract_text(path_to_file)
    open("parsed_content.txt", "w").write(text)


def parse_pdf_for_tables(path_to_file):
    tables = camelot.read_pdf(path_to_file, pages='1,2,3')
    print("Total tables extracted:", tables.n)
    print(tables[0].df)
    #tables.export("foo.xlsx", f="xlsx", compress=True)
    tables.export("foo.xlsx", f="excel")


def main():
    name_of_file = "candidates_samsmu.pdf"
    download_pdf(URL_TO_SAMSMU_PDF, name_of_file)
    #find_numbers_of_people_up_to_target(name_of_file)
    parse_pdf_for_tables(name_of_file)


if __name__ == '__main__':
    main()
